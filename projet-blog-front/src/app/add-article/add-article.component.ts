import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {

  article:Article = {
    title: '',
    image: '',
    content: ''
  };
  displayAddArticle:boolean = false;
  constructor(private articleService:ArticleService, private router:Router) { }

  ngOnInit(): void {
  }

  showAddArticleForm() {
    if (!this.displayAddArticle) {
      this.displayAddArticle = true;
    } else if (this.displayAddArticle) {
      this.displayAddArticle = false;;

    }

  }

  addArticle() {
    this.articleService.add(this.article).subscribe(() => {
      this.router.navigate(['/']);
    }); //On oublie pas le subscribe, sinon la requête ne sera pas lancée
  }
}
