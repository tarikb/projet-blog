import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddArticleComponent } from './add-article/add-article.component';
import { FormsModule } from '@angular/forms';
import { DeleteArticleComponent } from './delete-article/delete-article.component';
import { AdminComponent } from './admin/admin.component';
import { UpdateArticleComponent } from './update-article/update-article.component';
import { SingleArticleComponent } from './single-article/single-article.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddArticleComponent,
    DeleteArticleComponent,
    AdminComponent,
    UpdateArticleComponent,
    SingleArticleComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
