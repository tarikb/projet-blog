import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  articles:Article[] = []
  single?:Article;

  article:Article = {
    title: '',
    image: '',
    content: ''
  };
  displayAddArticle:boolean = false;
  displayEditArticle:boolean =false;
  displayDeleteArticle:boolean = false;
  modifiedArticle:Article = {title: "", image:"", content:""};
dropDownUpdate= 1;
  constructor(private articleService:ArticleService, private router:Router) { }

  ngOnInit(): void {
    this.articleService.getAll().subscribe(data => this.articles = data);

  }

  showAddArticleForm() {
    if (!this.displayAddArticle) {
      this.displayAddArticle = true;
    } else if (this.displayAddArticle) {
      this.displayAddArticle = false;;

    }

  }

  showEditArticleForm() {
    if (!this.displayEditArticle) {
      this.displayEditArticle = true;
    } else if (this.displayEditArticle) {
      this.displayEditArticle = false;;

    }

  }

  showDeleteArticleForm() {
    if (!this.displayDeleteArticle) {
      this.displayDeleteArticle = true;
    } else if (this.displayDeleteArticle) {
      this.displayDeleteArticle = false;;

    }

  }

  addArticle() {
    this.articleService.add(this.article).subscribe(() => {
      this.router.navigate(['/']);
    }); //On oublie pas le subscribe, sinon la requête ne sera pas lancée
  }

  showEditForm(article:Article) {
    this.displayEditArticle = true;
    // Impossible, le = copie juste la référence, il ne fait pas une copie
    // this.modifiedOp = operation;
    this.modifiedArticle = Object.assign({},article);
  }

  update(article:Article) {
    // Envoyer au serveur
    this.articleService.put(article).subscribe();
    // Mettre à jour la liste en HTML
    this.updateArticleInList(article);
    // Fermer le formulaire
    this.displayEditArticle = false;
    this.router.navigate(['/']);

  }

  updateArticleInList(article:Article) {
    this.articles.forEach((article_list) => {
      if(article_list.id == article.id) {
        article_list.title = article.title;
        article_list.date = article.date;
        article_list.content = article.content;
      }
    });
  }



  delete(id:number) {
    console.log(id);
    
    this.articleService.delete(id).subscribe();
    this.router.navigate(['/']);

    // this.removeFromList(id);
  }

  // removeFromList(id: number) {
  //   this.articles.forEach((article, article_index) => {
  //     if(article.id == id) {
  //       this.articles.splice(article_index, 1);
  //     }
  //   });
  // }
}
