import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddArticleComponent } from './add-article/add-article.component';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './home/home.component';
import { SingleArticleComponent } from './single-article/single-article.component';
import { UpdateArticleComponent } from './update-article/update-article.component';

const routes: Routes = [
  {path: '', component: HomeComponent },
  // {path: 'admin', component: AddArticleComponent },
  // {path: 'admin', component: UpdateArticleComponent },
  {path: 'admin', component: AdminComponent },
  {path:'article/:id', component: SingleArticleComponent},


  {path: 'add-article', component: AddArticleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
