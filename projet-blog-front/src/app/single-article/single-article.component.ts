import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {
article?:Article;
comment?:Comment;
  constructor(private articleService:ArticleService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.articleService.getById(params['id']))
    )
    .subscribe(data => this.article = data);
  }

}
