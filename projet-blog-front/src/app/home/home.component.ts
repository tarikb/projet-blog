import { Component, Input, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  articles:Article[] = []
  single?:Article;
  displayEditForm:boolean = false;
  modifiedArticle:Article = {title: "", image:"", content:""};
  constructor(private articleService:ArticleService) { }

  ngOnInit(): void {
    this.articleService.getAll().subscribe(data => this.articles = data);

  }

  fetchOne() {
    this.articleService.getById(1).subscribe(data => {
      this.single = data
      console.log(this.single)
    });
  }


  delete(id:number) {
    this.articleService.delete(id).subscribe();
    this.removeFromList(id);
  }

  removeFromList(id: number) {
    this.articles.forEach((article, article_index) => {
      if(article.id == id) {
        this.articles.splice(article_index, 1);
      }
    });
  }

  fetchAll(){
    this.articleService.getAll().subscribe(data => this.articles = data);
  }

  showEditForm(article:Article) {
    this.displayEditForm = true;
    // Impossible, le = copie juste la référence, il ne fait pas une copie
    // this.modifiedOp = operation;
    this.modifiedArticle = Object.assign({},article);
  }

  update(article:Article) {
    // Envoyer au serveur
    this.articleService.put(article).subscribe();
    // Mettre à jour la liste en HTML
    this.updateArticleInList(article);
    // Fermer le formulaire
    this.displayEditForm = false;
  }

  updateArticleInList(article:Article) {
    this.articles.forEach((article_list) => {
      if(article_list.id == article.id) {
        article_list.title = article.title;
        article_list.date = article.date;
        article_list.content = article.content;
      }
    });
  }
}
