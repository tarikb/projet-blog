export interface Comment {
    id:number;
    content:string;
    date:string;
}

export interface Article {
    id?:number;
    title:string;
    image:string;
    content:string;
    date?:string;
    comments?:Comment[];
}

