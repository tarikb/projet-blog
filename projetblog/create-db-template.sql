CREATE DATABASE blog DEFAULT CHARACTER SET = 'utf8mb4';

USE blog;

DROP TABLE IF EXISTS article;

CREATE TABLE article (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	title VARCHAR(20),
	image VARCHAR(128),
	content TEXT,
	date DATE
);

INSERT INTO
	article (title, image, content, date)
VALUES
	(
		"titre 1",
		"lien de limage 1",
		"Contenu de l'article 1",
		"2022-01-01"
	),
	(
		"titre 2",
		"lien de limage 2",
		"Contenu de l'article 2",
		"2022-02-02"
	),
	(
		"titre 3",
		"lien de limage 3",
		"Contenu de l'article 3",
		"2022-03-03"
	);


SELECT
	*
FROM
	article;

DROP TABLE IF EXISTS comment;

CREATE TABLE comment (
		id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
		content TEXT,
		date DATE,
		id_article INT UNSIGNED,
  CONSTRAINT FK_article FOREIGN KEY (id_article) REFERENCES article(id) ON DELETE CASCADE

	);

INSERT INTO
	comment (content, date, id_article)
VALUES
	(
		"Contenu du commentaire 1",
		"2022-01-01",
		1
	),
	(
		"Contenu du commentaire 2",
		"2022-02-02",
		1
	),
	(
		"Contenu du commentaire 3",
		"2022-03-03",
		3
	);

	SELECT * from comment WHERE id_article = 3;

	SELECT * FROM article;

	SELECT *
FROM article
INNER JOIN comment ON article.id = comment.id_article
WHERE article.id=1;

select * from comment
inner join article on article.id=comment.id_article
WHERE article.id=1;
select * from article;

select * from comment inner join article on article.id=comment.id_article WHERE article.id=3;