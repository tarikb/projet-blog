package co.simplon.p18.projetblog.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class articleEntity {
    private int id;
    private String title;
    private String image;
    private String content;
    private LocalDate date;
    private List<commentEntity> comments = new ArrayList<>();
    public articleEntity() {
    }
    public articleEntity(String title, String image, String content, LocalDate date) {
        this.title = title;
        this.image = image;
        this.content = content;
        this.date = date;
    }
    public articleEntity(int id, String title, String image, String content, LocalDate date) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.content = content;
        this.date = date;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public List<commentEntity> getcomments() {
        return comments;
    }
    public void setComments(List<commentEntity> comments) {
        this.comments = comments;
    }
    public void addComment(commentEntity comment) {
        this.comments.add(comment);
    }
}
