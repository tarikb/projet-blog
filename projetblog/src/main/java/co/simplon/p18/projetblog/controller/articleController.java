package co.simplon.p18.projetblog.controller;

import java.time.LocalDate;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetblog.entity.articleEntity;
import co.simplon.p18.projetblog.repository.articleRepository;
import co.simplon.p18.projetblog.repository.commentRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class articleController {
    @Autowired
    articleRepository repo;
    @Autowired
    private commentRepository commentRepo;

    @GetMapping("/api/articles")
    public List<articleEntity> all() {
        return repo.findAll();
    }

    @Autowired
    private DataSource dataSource;

    @PostMapping("/api/article")
    public articleEntity add(@RequestBody articleEntity article) {
        if (article.getDate() == null) {
            article.setDate(LocalDate.now());
        }

        repo.saveArticle(article);
        return article;
    }

    @GetMapping("/api/article/{id}") //
    public articleEntity one(@PathVariable int id) {
        articleEntity article = repo.findById(id);
        if (article == null) { 
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        article.setComments(commentRepo.findByIdArticle(id));

        return article;
    }

    @DeleteMapping("/api/article/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteArticle(@PathVariable int id) {
         if(!repo.deleteById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/api/article/{id}")
    public articleEntity update(@PathVariable int id,
     @RequestBody articleEntity article) {
         if (id != article.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }
        if(!repo.update(article)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(article.getId());
    }
}