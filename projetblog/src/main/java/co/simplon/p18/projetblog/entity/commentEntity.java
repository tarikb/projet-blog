package co.simplon.p18.projetblog.entity;

import java.time.LocalDate;

public class commentEntity {
    private int id;
    private String content;
    private LocalDate date;
    public commentEntity() {
    }
    public commentEntity(String content, LocalDate date) {
        this.content = content;
        this.date = date;
    }
    public commentEntity(int id, String content, LocalDate date) {
        this.id = id;
        this.content = content;
        this.date = date;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
}
