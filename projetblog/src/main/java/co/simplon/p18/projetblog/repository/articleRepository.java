package co.simplon.p18.projetblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetblog.entity.articleEntity;
import co.simplon.p18.projetblog.entity.commentEntity;

@Repository
public class articleRepository {
    @Autowired
    private DataSource dataSource;

    public List<articleEntity> findAll() {
        List<articleEntity> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                articleEntity article = new articleEntity(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("image"),
                        rs.getString("content"),
                        rs.getDate("date").toLocalDate());

                list.add(article);
            }


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }
    public void saveArticle(articleEntity article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO article (title, image, content, date) VALUES (?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getImage());
            stmt.setString(3, article.getContent());
            stmt.setDate(4, Date.valueOf(article.getDate()));

            stmt.executeUpdate();

            /**
             * cette partie là permet de récupérer l'id auto increment par la bdd et de
             * l'assigner à l'instance de Dog passée en argument
             */
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {

                article.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public articleEntity findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                articleEntity article = new articleEntity(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("image"),
                        rs.getString("content"),
                        rs.getDate("date").toLocalDate()
                        );

                return article;
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return null;
    }




    public boolean deleteById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    public boolean update(articleEntity article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET title=?,image=?,content=?,date=? WHERE id=?");
            
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getImage());
            stmt.setString(3, article.getContent());
            stmt.setDate(4, Date.valueOf(article.getDate()));
            stmt.setInt(5, article.getId());

            return stmt.executeUpdate() == 1;
            

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }


    public articleEntity findByIdWithComments(int id) {
        
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("	SELECT * FROM article");

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                articleEntity article = new articleEntity(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("image"),
                        rs.getString("content"),
                        rs.getDate("date").toLocalDate()
                        );
            PreparedStatement stmt2 = connection.prepareStatement("select * from comment inner join article on article.id=comment.id_article WHERE article.id=?");
            stmt2.setInt(1, id);
            ResultSet rs2 = stmt2.executeQuery();
            List<commentEntity> listComment=new ArrayList<>();
            while(rs2.next()==true){        
                commentEntity commentEntity=new commentEntity(rs2.getInt("id"),
                rs2.getString("content"),
                rs2.getDate("date").toLocalDate()
                );
                listComment.add(commentEntity);      
            };
            article.setComments(listComment); 
            return article;
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return null;
    }
    // SELECT * FROM article INNER JOIN comment ON article.id = comment.id_article WHERE article.id=?;"
}
